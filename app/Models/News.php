<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    /**
     * @var string
     */
    protected $table = 'news';

    /**
     * @var array
     */
    protected $fillable = ['title', 'alias', 'description', 'content', 'image', 'category_id', 'sort', 'active'];

}
