<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Repositories\NewsRepositoryInterface;

class NewsController extends Controller
{
    /**
     * @var $newsRepo
     */
    protected $newsRepo;

    /**
     * NewsController constructor.
     * @param NewsRepositoryInterface $newsRepo
     */
    public function __construct(NewsRepositoryInterface $newsRepo)
    {
        $this->newsRepo = $newsRepo;
    }

    /**
     * get news list
     * @return JsonResponse
     */
    public function list()
    {
        $results = $this->newsRepo->getAll();
        return response()->json(['users' => $results], 200);
    }

    /**
     * get news by id
     * @param $id
     * @return JsonResponse
     */
    public function find($id)
    {
        $results = $this->newsRepo->find($id);
        return response()->json(['user' => $results], 200);
    }

    /**
     * add new record
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        $data = $request->all();

        //... Validation here

        try {
            $this->newsRepo->create($data);
            //return successful response
            return response()->json(['news' => $data, 'message' => 'Created success!'], 201);
        } catch (Exception $e) {
            //return error message
            return response()->json(['message' => 'Create Failed!'], 409);
        }
    }

    /**
     * update record
     * @param Request $request
     * @param $id
     * @return JsonResponse
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();

        //... Validation here

        try {
            $this->newsRepo->update($id, $data);
            //return successful response
            return response()->json(['news' => $data, 'message' => 'Updated success!'], 201);
        } catch (Exception $e) {
            //return error message
            return response()->json(['message' => 'Update Failed!'], 409);
        }
    }

    /**
     * delete record
     * @param $id
     * @return JsonResponse
     */
    public function destroy($id)
    {
        try {
            $this->newsRepo->delete($id);
            //return successful response
            return response()->json(['message' => 'Deleted success!'], 201);
        } catch (Exception $e) {
            //return error message
            return response()->json(['message' => 'Delete Failed!'], 409);
        }
    }

}
