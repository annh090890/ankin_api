<?php


namespace App\Repositories;


interface NewsRepositoryInterface
{
    /**
     * @return mixed
     */
    public function getProduct();

    public function getAll();

}
