<?php


namespace App\Repositories;

use App\Models\News;

class NewsRepository extends BaseRepository implements NewsRepositoryInterface
{
    /**
     * @return mixed|string
     */
    public function getModel()
    {
        return News::class;
    }

    /**
     * @inheritDoc
     */
    public function getProduct()
    {
        return $this->model->select('title', 'description', 'content')->take(5)->get();
    }


}
