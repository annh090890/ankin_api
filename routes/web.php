<?php

/** @var Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use Laravel\Lumen\Routing\Router;

$router->get('/', function () use ($router) {
    return $router->app->version();
});

// API route group
$router->group(['prefix' => 'api'], function () use ($router) {
    // Matches "/api/register
    $router->post('register', 'AuthController@register');
    // Matches "/api/login
    $router->post('login', 'AuthController@login');

    // get profile by token
    $router->get('profile', 'UserController@profile');
    // get one user by id
    $router->get('users/{id}', 'UserController@singleUser');
    // get all users
    $router->get('users', 'UserController@allUsers');

    // News routes
    $router->post('create-news', 'NewsController@store');
    $router->get('news', 'NewsController@list');
    $router->put('news/{id}', 'NewsController@update');
    $router->delete('news', 'NewsController@destroy');
    $router->get('news/{id}', 'NewsController@find');

});
